<?php

class UserCommand extends CConsoleCommand
{
    /**
     * Set new password for users with a particular role
     *
     * @param $role string role name
     * @param $newPassLength int new password length
     * @return int
     */
    public function actionSetNewPassword($role, $newPassLength = 6)
    {
        $users = User::model()->with('authItems')->findAll(
            [
                'condition' => 'authItems.name=:role',
                'params' => [':role' => $role]
            ]
        );

        if (!$users) {
            fwrite(STDERR, "Users with role '$role' weren't found\n");
            return 1;
        }

        /**
         * @var $transaction CDbConnection
         */
        $transaction = Yii::app()->db->beginTransaction();

        foreach ($users as $user) {
            $user->password = User::model()->hashPassword(
                User::model()->generatePassword($newPassLength)
            );

            if (!$user->save()) {
                $transaction->rollback();
                fwrite(STDERR, var_export($user->getErrors(), true) . "\n");
                return 1;
            }
        }
        $transaction->commit();

        fwrite(STDOUT, 'Updated ' . count($users) . " rows\n");
        return 0;
    }
}