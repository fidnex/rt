<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath' => dirname(__DIR__),
	'name' =>'My Console Application',

	// application components
	'components' => array(

		// database settings are configured in database.php
		'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=rt',
            'emulatePrepare' => true,
            'username' => 'fidnex',
            'password' => '',
            'charset' => 'utf8'
        ),

	),
    'import' => array(
        'application.models.*',
    )
);
